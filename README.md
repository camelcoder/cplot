# cplot
A c89 OpenGL primitive plotter.

## Examples
![Hypercube](examples/hypercube/demo.gif)
![Perline noise terrain](examples/perline_terrain/demo.gif)

## License
View the licence file [LICENSE](LICENSE)
