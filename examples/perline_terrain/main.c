#define CPLOT_IMPLEMENT
#define CPLOT_SAMPLES 8
#include "../../cplot.h"
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>

#include "perlin.c"


int terrainWidth = 250;
int terrainHeight = 250;

struct CpPlot *init(void)
{
    struct CpPlot *p;
    GLuint *itIndices;
    QMvec3 *itVerts;
    int x, y;
    cp_init(800, 600);

    p = cp_add_plot(
            terrainHeight * terrainWidth,
            terrainHeight * terrainWidth + terrainWidth * (terrainHeight - 2),
            GL_TRIANGLE_STRIP,
            "#version 330 core\n"
            "in vec3 vPos;\n"
            "out vec4 fragColor;\n"

            "void main(void)\n"
            "{\n"
            "   fragColor = mix(\n"
            "       vec4(46.0 / 256.0, 84.0 / 256.0, 89.9 / 256.0, 1.0),\n"
            "       vec4(219.0 / 256.0, 167.0 / 256.0, 51.9 / 256.0, 1.0),\n"
            "       (vPos.y + 30.0) / 60.0);\n"
            "}\n"
    );
    cpCam.pos = qm_vec3(-terrainWidth, terrainHeight, -26.0f);
    cpCam.yaw = 30.0f;
    cpCam.pitch = -30.0f;

    itIndices = p->indices;
    for (y = 0; y < terrainHeight; ++y)
    {
        for (x = 0; x < terrainWidth; ++x)
        {
            *(itIndices)++ = (y * terrainHeight) + x;
            *(itIndices)++ = ((y + 1) * terrainHeight) + x;
        }
        ++y;
        if (y >= terrainHeight - 1) {
            break;
        }
        for (x = terrainWidth - 1; x >= 0; --x)
        {
            *(itIndices)++ = (y * terrainHeight) + x;
            *(itIndices)++ = ((y + 1) * terrainHeight) + x;
        }
    }
    assert((size_t)(itIndices - p->indices) ==  p->numIndecies);
    itVerts = p->verts;
    for (y = 0; y < terrainHeight; ++y)
    {
        for (x = 0; x < terrainWidth; ++x)
        {
            *(itVerts++) = qm_vec3(x, 0, y);
        }
    }
    cpCam.speed *= 15.0;

    /* glPolygonMode(GL_FRONT_AND_BACK, GL_LINE); */
    return p;
}

int main(void)
{
    struct CpPlot *p = init();

    float offset = 0;

    while (cp_update())
    {
        int x, y;
        float yoff = offset;
        QMvec3 *it = p->verts;
        for (y = 0; y < terrainHeight; ++y)
        {
            float xoff = 0;
            for (x = 0; x < terrainWidth; ++x)
            {
                (it++)->y = perlin_two(xoff, yoff, 1.0, 3, 1) * 80.0f;
                xoff += 0.02;
            }
            yoff += 0.02;
        }
        cp_plot_flush(p);
        cp_render_all();
        offset += cpDeltaTime;
    }

    cp_shutdown();

    return EXIT_SUCCESS;
}
