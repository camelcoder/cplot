#define CPLOT_IMPLEMENT
#define CPLOT_SAMPLES 8
#include "../../cplot.h"
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>

#define M_PI 3.14159265358979323846

QMvec4 points[16] = {
    { -1, -1, -1,  1},
    {  1, -1, -1,  1},
    {  1,  1, -1,  1},
    { -1,  1, -1,  1},
    { -1, -1,  1,  1},
    {  1, -1,  1,  1},
    {  1,  1,  1,  1},
    { -1,  1,  1,  1},
    { -1, -1, -1, -1},
    {  1, -1, -1, -1},
    {  1,  1, -1, -1},
    { -1,  1, -1, -1},
    { -1, -1,  1, -1},
    {  1, -1,  1, -1},
    {  1,  1,  1, -1},
    { -1,  1,  1, -1},
};

struct CpPlot *init(void)
{
    struct CpPlot *p;
    cp_init(800, 600);

    cpCam.pos = qm_vec3(8.45, 2.0, -6.25);
    cpCam.yaw = -216.0;
    cpCam.pitch = -10.0;

    p = cp_add_plot(
            16,
            32 * 2,
            GL_LINES,
            "#version 330 core\n"
            "in vec3 vPos;\n"
            "out vec4 fragColor;\n"

            "void main(void)\n"
            "{\n"
            "   fragColor = vec4(normalize(vPos) / 2.0 + 0.5, 1);\n"
            "}\n"
    );

    /* glPolygonMode(GL_FRONT_AND_BACK, GL_LINE); */
    return p;
}

int main(void)
{
    struct CpPlot *p = init();

    while (cp_update())
    {
        float angle = glfwGetTime();
        GLuint *it;
        size_t i;

        QMmat4 rotationXY = qm_mat4_diag(1);
        QMmat4 rotationZW = qm_mat4_diag(1);
        rotationXY.m00 = cos(angle);
        rotationXY.m01 =-sin(angle);
        rotationXY.m10 = sin(angle);
        rotationXY.m11 = cos(angle);

        rotationXY.m22 = cos(angle);
        rotationXY.m23 =-sin(angle);
        rotationXY.m32 = sin(angle);
        rotationXY.m33 = cos(angle);

        it = p->indices;
        for (i = 0; i < 4; ++i)
        {
            *(it++) = i;
            *(it++) = (i + 1) % 4;
            *(it++) = i + 4;
            *(it++) = ((i + 1) % 4) + 4;
            *(it++) = i;
            *(it++) = i + 4;
        }
        for (i = 0; i < 4; ++i)
        {
            *(it++) = 8 + i;
            *(it++) = 8 + (i + 1) % 4;
            *(it++) = 8 + i + 4;
            *(it++) = 8 + ((i + 1) % 4) + 4;
            *(it++) = 8 + i;
            *(it++) = 8 + i + 4;
        }
        for (i = 0; i < 8; ++i)
        {
            *(it++) = i;
            *(it++) = i + 8;
        }

        glLineWidth(4.0);
        for (i = 0; i < p->numVertices; ++i)
        {

            QMvec4 rotated =
                qm_mul_m4v4(rotationZW,
                    qm_mul_m4v4(rotationXY, points[i]));
            float distance = 2;
            float w = 1 / (distance - rotated.w);
            QMmat4 projection = qm_mat4_diag(w);
            QMvec4 projected = qm_mul_m4v4(projection, rotated);

            p->verts[i].x = projected.x;
            p->verts[i].y = projected.y;
            p->verts[i].z = projected.z;
        }
        cp_plot_flush(p);
        cp_render_all();
    }

    cp_shutdown();

    return EXIT_SUCCESS;
}
