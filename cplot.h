#if !defined(CPLOT_H)

#if defined(CPLOT_IMPLEMENT)
    #define QUICK_MATH_IMPLEMENT
#endif
#include "quickmath/quickmath.h"
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#ifdef _DEBUG
# define GL_CALL(x)                                               \
    do {                                                          \
        GLenum errorCode = 0;                                     \
        while (glGetError()){}                                    \
        x;                                                        \
        while ((errorCode = glGetError()))                        \
        {                                                         \
            fprintf(stderr,                                       \
                    "[OpenGL Error] (%u): %s in %s on line %d\n", \
                    errorCode , #x, __FILE__, __LINE__);          \
            exit(EXIT_FAILURE);                                   \
        }                                                         \
    }                                                             \
    while (0)

#else
# define GL_CALL(x) do { x; } while (0)
#endif

/*
TODO:
    Don't do Cplot use globals
    change CpPlot to Cplot and prefix with
*/

struct CpCam
{
    QMmat4 proj, view;
    QMvec3 pos, front, up;
    float yaw, pitch;
    float speed, rotationSpeed;
};
struct CpCam cpCam = {
    {0}, {0},
    {1.0f, 1.0f, 3.0f},
    {0.0f, 0.0f, -1.0f},
    {0.0f, 1.0f, 0.0f},
    -90.0f,
    0.0f,
    5.0f,
    60.0f,
};

static float cpDeltaTime, cpLastFrame = 0.0f;


static GLFWwindow *cpWindow;
static int cpWindowWidth, cpWindowHeight;

static char const *cpVertexShader =
    "#version 330 core\n"
    "layout(location = 0) in vec3 aPos;\n"
    "out vec3 vPos;\n"
    "uniform mat4 view;\n"
    "uniform mat4 proj;\n"
    ""
    "void main(void)\n"
    "{\n"
    "    gl_Position = proj * view * vec4(aPos, 1);\n"
    "    vPos = aPos;\n"
    "}\n";
static char const *cpFragmentShaderDefault =
    "#version 330 core\n"
    "in vec3 vPos;\n"
    "out vec4 fragColor;\n"
    ""
    "void main(void)\n"
    "{\n"
    "   fragColor = vec4(1, 1, 1, 1);\n"
    "}\n";

static struct CpPlot
{
    struct CpPlot *next;
    struct CpPlot *prev;

    GLuint vao, vbo, shader;
    GLint projPos, viewPos;
    GLenum mode;

    size_t numIndecies; /* if 0 than there will be no ebo used */
    GLuint ebo;
    GLuint *indices;

    size_t numVertices;
    QMvec3 *verts;
} *cpPlot = {0};

extern void cp_init(int width, int height);
extern void cp_shutdown(void);
extern int cp_update_and_render(void);

extern struct CpPlot *cp_add_plot(
        size_t numVertices,
        size_t numIndecies, /* if 0 than there will be no ebo used */
        GLenum drawMode,
        char const *fragmentShader);
extern void cp_remove_plot(struct CpPlot *plot);
#define CPLOT_H
#endif

#if defined(CPLOT_IMPLEMENT)

static void cp_framebuffer_size_callback(GLFWwindow *wnd, int width, int height)
{
    struct CpPlot *it = cpPlot;
    (void)wnd;
    cpWindowWidth = width;
    cpWindowHeight = height;
    GL_CALL(glViewport(0, 0, width, height));

    cpCam.proj = qm_perspective(60.0f, (float)width / (float)height, 0.001f, 10000.0f);
    for (it = cpPlot; it; it = it->next)
    {
        GL_CALL(glUseProgram(it->shader));
        GL_CALL(glUniformMatrix4fv(it->projPos, 1, GL_FALSE, &cpCam.proj.m00));
    }
}

unsigned int cp_compile_shader(unsigned int type, const char *source)
{
    int success;
    unsigned int id;
    GL_CALL(id = glCreateShader(type));
    GL_CALL(glShaderSource(id, 1, &source, NULL));
    GL_CALL(glCompileShader(id));

    GL_CALL(glGetShaderiv(id, GL_COMPILE_STATUS, &success));
    if (!success)
    {
        int lenght;
        char *message;
        GL_CALL(glGetShaderiv(id, GL_INFO_LOG_LENGTH, &lenght));
        message = (char*)malloc(lenght * sizeof(char));
        GL_CALL(glGetShaderInfoLog(id, lenght, &lenght, message));
        GL_CALL(glDeleteShader(id));
        return 0;
    }

    return id;
}

GLuint cp_create_shader(const char *frag)
{
    GLuint program;
    unsigned int vs = cp_compile_shader(GL_VERTEX_SHADER, cpVertexShader);
    unsigned int fs = cp_compile_shader(GL_FRAGMENT_SHADER, frag);
    GL_CALL(program = glCreateProgram());

    GL_CALL(glAttachShader(program, vs));
    GL_CALL(glAttachShader(program, fs));
    GL_CALL(glLinkProgram(program));
    GL_CALL(glValidateProgram(program));

    GL_CALL(glDeleteShader(vs));
    GL_CALL(glDeleteShader(fs));

    return program;
}

void cp_init(int width, int height)
{
    (void)cpFragmentShaderDefault;
    cpWindowWidth = width;
    cpWindowHeight = height;

    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
#if CPLOT_SAMPLES > 0
    glfwWindowHint(GLFW_SAMPLES, CPLOT_SAMPLES);
#endif
#ifdef __APPLE__
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
#endif

    cpWindow = glfwCreateWindow(width, height, "cplot", NULL, NULL);
    if (!cpWindow) {
        fprintf(stderr, "glfwCreateWindow failed\n");
        exit(1);
    }
    glfwMakeContextCurrent(cpWindow);
    glfwSetFramebufferSizeCallback(cpWindow, cp_framebuffer_size_callback);

    glewExperimental = 1;
    glewInit();

#if CPLOT_SAMPLES > 0
    glEnable(GL_MULTISAMPLE);
#endif
    GL_CALL(glViewport(0, 0, width, height));
    GL_CALL(glEnable(GL_DEPTH_TEST));
    cpCam.proj = qm_perspective(60.0f, (float)width / (float)height, 0.001f, 10000.0f);
}
void cp_shutdown(void)
{
    struct CpPlot *next = cpPlot;
    while (cpPlot)
    {
        next = cpPlot->next;
        free(cpPlot);
        cpPlot = next;
    }

    glfwDestroyWindow(cpWindow);
    glfwTerminate();
}
int cp_update(void)
{
    float speed, rotationSpeed;
    const float currentFrame = glfwGetTime();
    cpDeltaTime = currentFrame - cpLastFrame;
    cpLastFrame = currentFrame;

    glfwPollEvents();

    if (glfwGetKey(cpWindow, GLFW_KEY_ESCAPE) == GLFW_PRESS)
        glfwSetWindowShouldClose(cpWindow, 1);

    speed = cpCam.speed * cpDeltaTime;
    rotationSpeed = cpCam.rotationSpeed * cpDeltaTime;

    if (glfwGetKey(cpWindow, GLFW_KEY_W) == GLFW_PRESS)
        cpCam.pos = qm_add_v3(cpCam.pos, qm_mul_v3f(cpCam.front, speed));
    if (glfwGetKey(cpWindow, GLFW_KEY_S) == GLFW_PRESS)
        cpCam.pos = qm_sub_v3(cpCam.pos, qm_mul_v3f(cpCam.front, speed));

    if (glfwGetKey(cpWindow, GLFW_KEY_A) == GLFW_PRESS)
        cpCam.pos = qm_sub_v3(cpCam.pos, qm_mul_v3f(qm_cross_v3(cpCam.front, cpCam.up), speed));
    if (glfwGetKey(cpWindow, GLFW_KEY_D) == GLFW_PRESS)
        cpCam.pos = qm_add_v3(cpCam.pos, qm_mul_v3f(qm_cross_v3(cpCam.front, cpCam.up), speed));

    if (glfwGetKey(cpWindow, GLFW_KEY_SPACE) == GLFW_PRESS)
        cpCam.pos = qm_add_v3(cpCam.pos, qm_mul_v3f(cpCam.up, speed));
    if (glfwGetKey(cpWindow, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS)
        cpCam.pos = qm_sub_v3(cpCam.pos, qm_mul_v3f(cpCam.up, speed));


    if (glfwGetKey(cpWindow, GLFW_KEY_H) == GLFW_PRESS)
        cpCam.yaw -= rotationSpeed;
    if (glfwGetKey(cpWindow, GLFW_KEY_L) == GLFW_PRESS)
        cpCam.yaw += rotationSpeed;

    if (glfwGetKey(cpWindow, GLFW_KEY_K) == GLFW_PRESS)
        cpCam.pitch += rotationSpeed;
    if (glfwGetKey(cpWindow, GLFW_KEY_J) == GLFW_PRESS)
        cpCam.pitch -= rotationSpeed;
    if(cpCam.pitch < -89.0f)
        cpCam.pitch = -89.0f;
    if(cpCam.pitch > 89.0f)
        cpCam.pitch = 89.0f;

    if (glfwGetKey(cpWindow, GLFW_KEY_Q) == GLFW_PRESS)
        cpCam.speed *= 1.05f;
    if (glfwGetKey(cpWindow, GLFW_KEY_E) == GLFW_PRESS)
        cpCam.speed *= 0.95f;

    if (1)
    {
        static int rep = 1;
        if (glfwGetKey(cpWindow, GLFW_KEY_P) == GLFW_PRESS)
        {
            if (rep) {
                printf("pos(%f, %f, %f)\n", cpCam.pos.x, cpCam.pos.y, cpCam.pos.z);
                rep = 0;
            }
        }
        else
            rep = 1;
    }

    cpCam.front = qm_normalize_v3(qm_vec3(
                qm_cos(qm_f_to_radians(cpCam.yaw)) * qm_cos(qm_f_to_radians(cpCam.pitch)),
                qm_sin(qm_f_to_radians(cpCam.pitch)),
                qm_sin(qm_f_to_radians(cpCam.yaw)) * qm_cos(qm_f_to_radians(cpCam.pitch))
                ));

    cpCam.view = qm_lookat(cpCam.pos, qm_add_v3(cpCam.pos, cpCam.front), cpCam.up);

    return !glfwWindowShouldClose(cpWindow);
}
void cp_render_all(void)
{
    struct CpPlot *it = cpPlot;

    GL_CALL(glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT));

    for (it = cpPlot; it; it = it->next)
    {
            GL_CALL(glUseProgram(it->shader));
            GL_CALL(glBindVertexArray(it->vao));
            GL_CALL(glBindBuffer(GL_ARRAY_BUFFER, it->vbo));

            GL_CALL(glUniformMatrix4fv(
                        it->viewPos, 1, GL_FALSE, &cpCam.view.m00));

        if (it->numIndecies == 0) {
            GL_CALL(glDrawArrays(it->mode, 0, it->numVertices));
        }
        else
        {
            GL_CALL(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, it->ebo));
            GL_CALL(glDrawElements(
                        it->mode,
                        it->numIndecies,
                        GL_UNSIGNED_INT,
                        0));
        }
    }

    glfwSwapBuffers(cpWindow);
}

struct CpPlot *cp_add_plot(
        size_t numVertices,
        size_t numIndecies, /* if 0 than there will be no ebo used */
        GLenum drawMode,
        char const *fragmentShader)
{
    struct CpPlot *ret = cpPlot;
    if (!ret)
    {
        ret = cpPlot = calloc(1, (sizeof *ret) +
                sizeof(QMvec3) * numVertices +
                sizeof(GLuint) * numIndecies);
    }
    else
    {
        while (ret->next) {
            ret = ret->next;
        }
        ret->next = calloc(1, (sizeof *ret) +
                sizeof(QMvec3) * numVertices +
                sizeof(GLuint) * numIndecies);
        ret = ret->next;
        ret->prev = ret;
    }

    ret->numVertices = numVertices;
    ret->numIndecies = numIndecies;
    ret->verts = (QMvec3*)(ret + 1);
    ret->mode = drawMode;
    ret->shader = cp_create_shader(fragmentShader);

    GL_CALL(glGenVertexArrays(1, &ret->vao));
    GL_CALL(glGenBuffers(1, &ret->vbo));
    /* VAO */
    GL_CALL(glBindVertexArray(ret->vao));
    /* VBO */
    GL_CALL(glBindBuffer(GL_ARRAY_BUFFER, ret->vbo));
    GL_CALL(glBufferData(GL_ARRAY_BUFFER, numVertices * sizeof(QMvec3), ret->verts, GL_STATIC_DRAW));
    /* EBO */
    if (numIndecies != 0)
    {
        ret->indices = (GLuint*)(ret->verts + ret->numVertices);
        GL_CALL(glGenBuffers(1, &ret->ebo));
        GL_CALL(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ret->ebo));
        GL_CALL(glBufferData(
                    GL_ELEMENT_ARRAY_BUFFER,
                    sizeof(GLuint) * ret->numIndecies,
                    ret->indices,
                    GL_STATIC_DRAW));
    }
    /* Vertex Attribs */
    GL_CALL(glEnableVertexAttribArray(0));
    GL_CALL(glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(QMvec3), (void*)0));

    GL_CALL(ret->viewPos = glGetUniformLocation(ret->shader, "view"));
    GL_CALL(ret->projPos = glGetUniformLocation(ret->shader, "proj"));

    GL_CALL(glUseProgram(ret->shader));
    GL_CALL(glUniformMatrix4fv(ret->projPos, 1, GL_FALSE, &cpCam.proj.m00));

    return ret;
}
void cp_remove_plot(struct CpPlot *plot)
{
    if (plot->prev) {
        plot->prev->next = plot->next;
    }
    if (plot->next) {
        plot->next->prev = plot->prev;
    }
    free(plot);
}
void cp_plot_flush(struct CpPlot *plot)
{
    GL_CALL(glBindBuffer(GL_ARRAY_BUFFER, plot->vbo));

    GL_CALL(glBufferSubData(
                GL_ARRAY_BUFFER,
                0,
                sizeof(QMvec3) * plot->numVertices,
                plot->verts));
    if (plot->numIndecies != 0)
    {
        GL_CALL(glBufferSubData(
                    GL_ELEMENT_ARRAY_BUFFER,
                    0,
                    sizeof(GLuint) * plot->numIndecies,
                    plot->indices));
    }
}

#endif /* CPLOT_IMPLEMENT */
